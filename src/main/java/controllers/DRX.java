package controllers;

import javafx.scene.paint.Color;
import model.WriteToFile;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

public class DRX {
    private double A = 8.6711e13;
    private double B = 9.412682e0;
    private double time = 0;

    private int rows;
    private int columns;
    private int packages = 50;

    private int tempRow;
    private int tempColumn;

    private double roCrit;
    private double prevRo;


    private String bc = "periodyczne";

    private ArrayList<Cell> onBound;
    private ArrayList<Cell> notOnBound;
    private ArrayList<Cell> neighbours = new ArrayList<>();

    private Cell[][] grid;
    DecimalFormat df = new DecimalFormat("0.000");
    WriteToFile writing = new WriteToFile();


    public DRX(int rows, int columns){
        this.rows = rows;
        this. columns = columns;
        roCrit = 4.21584e12/(rows*columns);
    }

    public void start(){ writing.start(); }

    public void stop(){
        writing.stop();
    }

    public void nextStep(){

        double currentRo = (A/B+(1-A/B)* Math.exp(-B*time));
        double ro = currentRo - prevRo;
        prevRo = currentRo;

        double sumRo = 0;

        double inCell = ro/(rows*columns);
        double x = 0.3 * inCell;
        for(int i = 0; i < rows; i++ ){
            for( int j = 0; j< columns; j++){
                grid[i][j].addRo(x);
                ro -= x;
                sumRo+=x;
            }
        }

        double inPack = ro/packages;
        Random rand = new Random();

        for(int i =0; i < packages; i++){
            int r;
            int random = rand.nextInt(100);
            if(random <= 20){
                r = rand.nextInt(notOnBound.size());
                notOnBound.get(r).addRo(inPack);
            }
            else{
                r = rand.nextInt(onBound.size());
                onBound.get(r).addRo(inPack);
            }
            sumRo+=inPack;
        }

        for(int i = 0; i < rows; i++){
            for(int j = 0; j< columns; j++){
                if(grid[i][j].getRo() > roCrit && grid[i][j].isOnBound()){
                    grid[i][j].setRecrystallized(true);
                    grid[i][j].setRo(0);
                    grid[i][j].setColor(getRedColor());
                }
            }
        }

        //growth
        for(int i = 0; i<rows; i++){
            for(int j = 0; j <columns; j++){
                checkNeighbours(i,j);
            }
        }

        writing.write(df.format(time) + " " + sumRo);
        time+=0.001;
    }

    public Color getRedColor(){
        Random random = new Random();
        int r = random.nextInt(255);
        return Color.rgb(r, 0, 0);
    }


    public void checkNeighbours(int row, int column){
        neighbours.clear();
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                if ((i != -1 && j != -1) || (i != 1 && j != 1) || (i!=-1&&j!=1)||(i!=1&&j!=-1) ) {
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }

        for(Cell cell : neighbours){
            if(!cell.isRecrystallized()){
                continue;
            }
            double ro = 0;
            for(Cell n : neighbours){
                if(cell!=n){
                    ro+=n.getRo();
                }
            }
            if(grid[row][column].getRo()>ro){
                grid[row][column].setRecrystallized(true);
                grid[row][column].setRo(0);
                grid[row][column].setColor(cell.getColor());
            }
        }

    }

    public void checkBC(int row, int column, int i, int j){
        if(bc.equals("periodyczne")) {
            if ((row + i) < 0) {
                tempRow = row + i + rows;
            }
            if ((row + i) >= rows) {
                tempRow = row + i - rows;
            }
            if ((column + j) < 0) {
                tempColumn = column + j + columns;
            }
            if ((column + j) >= columns) {
                tempColumn = column + j - columns;
            }
        }
        else if(bc.equals("absorbujące")) {
            if ((row + i) < 0) {
                tempRow = 0;
            }
            if ((row + i) >= rows) {
                tempRow = row;
            }
            if ((column + j) < 0) {
                tempColumn = 0;
            }
            if ((column + j) >= columns) {
                tempColumn = column;
            }
        }
    }

    public void initArrays(){
        onBound = new ArrayList<>();
        notOnBound = new ArrayList<>();
        for(int i = 0; i < rows; i++ ){
            for( int j = 0; j< columns; j++){
                if (grid[i][j].isOnBound()) {
                    onBound.add(grid[i][j]);
                } else {
                    notOnBound.add(grid[i][j]);
                }
            }
        }
    }


    public void update(Cell[][] grid, int rows, int columns){
        this.grid = grid;
        this.rows = rows;
        this.columns = columns;
        roCrit =(A/B+(1-A/B)* Math.exp(-B*0.65))/(rows*columns);
        prevRo = 0;
        time = 0.000;
        initArrays();
    }


}

