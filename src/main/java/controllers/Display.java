package controllers;

import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.Random;

public class Display {

    private TilePane tilePane = new TilePane();
    private int row;
    private int column;
    private double cellSize;


    public Display(int row, int column, double cellSize, Grid grid){

        this.row = row;
        this.column = column;
        this.cellSize = cellSize;
        tilePane.getChildren().clear();
        tilePane.setPrefColumns(column);
        tilePane.setPrefRows(row);


        Cell[][] cells = grid.getGrid();
        for(int i = 0; i < row; i++){
            for(int j = 0; j < column; j++){
                Color color = cells[i][j].getColor();
                Rectangle rectangle = new Rectangle(cellSize, cellSize, color);
                tilePane.getChildren().add(rectangle);
                attachListener(rectangle, cells[i][j]);
            }
        }

    }

    public void displayGrid(Grid grid){
        Cell[][] cells = grid.getGrid();
        for(int i = 0; i < row; i++){
            for(int j = 0; j < column; j++){
                Rectangle rectangle = (Rectangle)tilePane.getChildren().get(i* column +j);
                rectangle.setFill(cells[i][j].getColor());
            }
        }
    }

    public void displayEnergy(Grid grid){
        Cell[][] cells = grid.getGrid();
        for(int i = 0; i < row; i++){
            for(int j = 0; j < column; j++){
                Rectangle rectangle = (Rectangle)tilePane.getChildren().get(i* column +j);
                rectangle.setFill(getEnergColor(cells[i][j]));
            }
        }
    }

    public void displayRo(Grid grid){
        Cell[][] cells = grid.getGrid();
        for(int i = 0; i < row; i++){
            for(int j = 0; j < column; j++){
                Rectangle rectangle = (Rectangle)tilePane.getChildren().get(i* column +j);
                rectangle.setFill((cells[i][j].isRecrystallized())?Color.web("#821011") : Color.web("#e1eec3"));
            }
        }
    }

    public void attachListener(Rectangle rectangle, Cell cell){
        rectangle.setOnMouseClicked(e -> {
            Color color = randomColor();
            rectangle.setFill(cell.getState() ? Color.WHITE : color);
            if(cell.getState()){
                cell.setColor(Color.WHITE);
            }
            else{
                cell.setColor(color);
            }
            cell.setState(!cell.getState());
        });
    }

    public Color randomColor(){
        Random random = new Random();
        int r = random.nextInt(255);
        int g = random.nextInt(255);
        int b = random.nextInt(255);
        return Color.rgb(r, g, b);
    }

    public Color getEnergColor(Cell cell){
        Color color = Color.web("#e1eec3");
        if(cell.getEnergy() == 1) color = Color.web("#f05053");
        else if(cell.getEnergy() == 2) color = Color.web("#db3f42");
        else if(cell.getEnergy() == 3) color = Color.web("#c43335");
        else if(cell.getEnergy() == 4) color = Color.web("#af2628");
        else if(cell.getEnergy() == 5) color = Color.web("#a31f20");
        else if(cell.getEnergy() == 6) color = Color.web("#931718");
        else if(cell.getEnergy() == 7) color = Color.web("#821011");
        else if(cell.getEnergy() == 8) color = Color.web("#6b0708");
        return color;
    }

    public TilePane getTilePane(){
        return tilePane;
    }


}
