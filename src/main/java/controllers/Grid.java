package controllers;

import controllers.Cell;
import javafx.scene.control.Alert;
import javafx.scene.paint.Color;
import java.util.ArrayList;
import java.util.Random;

public class Grid {

    private int rows;
    private int columns;
    private int numberOfSeeds;
    private int seedsInRow;
    private int seedsInColumn;
    private int radius;
    private Random random;
    private ArrayList<Integer> randX;
    private ArrayList<Integer> randY;
    private String nucleation;
    private Cell[][] grid;

    public Grid(int rows, int columns, String nucleation){
        this.rows = rows;
        this.columns = columns;
        this.nucleation = nucleation;
        initializeGrid();
    }

    public void initializeGrid(){
        random = new Random();
        grid = new Cell[rows][columns];
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j++){
                double x = -1 + (random.nextDouble() * 2);
                double y = -1 + (random.nextDouble() * 2);
                grid[i][j] = new Cell();
                grid[i][j].setCenter(x,y);
            }
        }
    }

    public void nucleation(){

        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j++){
                if(grid[i][j].getState()) {
                    grid[i][j].setState(false);
                    grid[i][j].setColor(Color.WHITE);
                }
            }
        }

        if(nucleation.equals("jednorodne")){
            int inRows = seedsInRow;
            int inColumns = seedsInColumn;
            for(int i = 1; i <= inRows; i++){
                for(int j = 1; j <= inColumns; j++){
                    grid[rows/(inRows+1)*i][columns/(inColumns+1)*j].setState(true);
                    grid[rows/(inRows+1)*i][columns/(inColumns+1)*j].setColor(randomColor());
                }
            }
        }
        else if(nucleation.equals("z promieniem")){

            randX = new ArrayList<>();
            randY = new ArrayList<>();
            random = new Random();
            int x = random.nextInt(rows);
            int y = random.nextInt(columns);
            randX.add(x);
            randY.add(y);
            grid[x][y].setState(true);
            grid[x][y].setColor(randomColor());
            for(int i = 1; i < numberOfSeeds; i++){
                boolean isAvailable = getRandomSeed();
                if(!isAvailable) break;
            }

        }
        else if(nucleation.equals("losowe")){
            for(int i = 0; i < numberOfSeeds; i++){
                if(!getRandomSeed()) i--;
            }

        }

    }

    public boolean getRandomSeed(){
        random = new Random();
        if(nucleation.equals("z promieniem")) {
            double distance = 0;
            double count = 0;
            int x, y, x1, y1;
            do {
                count++;
                x = random.nextInt(rows);
                y = random.nextInt(columns);
                for (int j = 0; j < randX.size(); j++) {
                    x1 = randX.get(j);
                    y1 = randY.get(j);
                    distance = Math.sqrt((Math.pow((x - x1), 2) + Math.pow((y - y1), 2)));
                    if (distance <= radius) break;
                }
                if (count == rows * columns) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Makzymalna ilość ziaren.");
                    alert.setContentText("Osiągnięto maksymalną ilość ziaren.");
                    alert.showAndWait();
                    return false;
                }
            } while (distance <= radius);
            randX.add(x);
            randY.add(y);
            grid[x][y].setState(true);
            grid[x][y].setColor(randomColor());
            return true;
        }
        else {
            int x = random.nextInt(rows);
            int y = random.nextInt(columns);
            if(!grid[x][y].getState()) {
                grid[x][y].setState(true);
                grid[x][y].setColor(randomColor());
                return true;
            }
            return false;
        }
    }


    public Color randomColor(){
        Random random = new Random();
        int r = random.nextInt(255);
        int g = random.nextInt(255);
        int b = random.nextInt(255);
        return Color.rgb(r, g, b);
    }

    public void setNumber(int number){
        this.numberOfSeeds = number;
    }

    public void setSeedsInRow(int number){
        this.seedsInRow = number;
    }

    public void setSeedsInColumn(int number){
        this.seedsInColumn = number;
    }

    public void setRadius(int radius){
        this.radius = radius;
    }

    public void setNucleation(String nucleation){
        this.nucleation = nucleation;
    }

    public Cell[][] getGrid(){
        return grid;
    }

}
