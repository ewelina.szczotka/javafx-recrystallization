package controllers;

import javafx.scene.paint.Color;
import model.Neighbours;
import model.OnBound;

import java.util.*;

public class CellularAutomaton {

    private int rows;
    private int columns;
    private Cell[][] grid;
    private ArrayList<Cell> listOfNeighbours = new ArrayList<>();
    private HashMap<Color,Integer> neighbourColors = new HashMap<>();

    private Neighbours neighbours;


    public void nextStep(){
        checkStates();
        updateBoard();
    }

    public void checkStates(){
        for(int i = 0; i < rows; i++){
            for(int j = 0; j< columns; j++){
                if(!grid[i][j].getState()) {
                    neighbours.addNeighbours(listOfNeighbours,i,j);
                    updateCell(i,j);
                }
            }
        }
    }

    public void updateCell(int row, int column){
        for(int i = 0; i < listOfNeighbours.size(); i++){
            if(listOfNeighbours.get(i).getState()){
                Color color = listOfNeighbours.get(i).getColor();
                int count = neighbourColors.containsKey(color) ? neighbourColors.get(color) : 0;
                neighbourColors.put(color, count + 1);
            }
        }
        if(!neighbourColors.isEmpty()){
            Color color = Collections.max(neighbourColors.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey();
            grid[row][column].setNewColor(color);
            grid[row][column].setNewState(true);
            neighbourColors.clear();
        }
    }

    public void updateBoard(){
        for(int i = 0; i < rows; i++){
            for(int j = 0; j< columns; j++){
                if(!grid[i][j].getState()) {
                    grid[i][j].updateNewState();
                    grid[i][j].updateNewColor();
                }
            }
        }
    }

    public boolean checkFinished(){
        for(int i = 0; i < rows; i++){
            for(int j = 0; j< columns; j++){
                if(!grid[i][j].getState()) {
                    return false;
                }
            }
        }
        setOnBound();
        return true;
    }

    public void update(Cell[][] grid, int rows, int columns, String neighbourhood, String bc, int radius, double cellSize){
        this.grid = grid;
        this.rows = rows;
        this.columns = columns;
        neighbours = new Neighbours(grid, neighbourhood, bc, cellSize, radius);
    }

    public Cell[][] getGrid(){
        return grid;
    }

    public void setOnBound(){
        OnBound.setOnBound(grid,neighbours);
    }
}
