package controllers;

import model.Neighbours;
import model.OnBound;

import java.util.*;

public class MonteCarlo {

    private Cell[][] grid;
    private Neighbours neighbours;
    private int rows, columns;
    private double kt;
    private Random random;
    ArrayList<Integer> index = new ArrayList<>();
    ArrayList<Cell> listOfNeighbours = new ArrayList<>();


    public void init(Cell[][] grid, int rows, int columns, double kt, String bc, String neighbourhood){
        index.clear();
        this.rows = rows;
        this.columns = columns;
        for(int i=0;i<rows*columns; i++){
            index.add(i);
        }
        this.grid = grid;
        this.kt = kt;
        neighbours = new Neighbours(grid, neighbourhood, bc);
    }

    public void nextStep(){

        Collections.shuffle(index);

        for(int i = 0; i<rows*columns; i++){

            int d = index.get(i);
            int row = d / columns;
            int column = d % rows;

            neighbours.addNeighbours(listOfNeighbours, row,column);

            int energyBefore = countEnergyBefore(row,column);
            int energyAfter = countEnergyAfter(row,column);
            int dEnergy = energyAfter - energyBefore;

            if(dEnergy<=0){
                grid[row][column].updateNewColor();
                grid[row][column].setEnergy(energyAfter);
            }
            else{
                random = new Random();
                double p = Math.exp(-(dEnergy/kt));
                double x = random.nextDouble();
                if (x<p){
                    grid[row][column].updateNewColor();
                    grid[row][column].setEnergy(energyAfter);
                }
                else{
                    grid[row][column].setEnergy(energyBefore);
                }
            }
        }

    }

    public int countEnergyBefore(int row, int column){
        int energy = 0;
        for(int i = 0; i< listOfNeighbours.size(); i++){
            if(listOfNeighbours.get(i).getColor() != grid[row][column].getColor()){
                energy++;
            }
        }
        return energy;
    }

    public int countEnergyAfter(int row, int column){
        int energy = 0;
        Collections.shuffle(listOfNeighbours);
        grid[row][column].setNewColor(listOfNeighbours.get(0).getColor());
        for(int i = 0; i< listOfNeighbours.size(); i++){
            if(listOfNeighbours.get(i).getColor() != grid[row][column].getNewColor()){
                energy++;
            }
        }
        return energy;
    }

    public void setOnBound(){
        OnBound.setOnBound(grid,neighbours);
    }

}
