package model;

import controllers.Cell;

import java.util.ArrayList;

public class OnBound {

    public static void setOnBound(Cell[][] grid, Neighbours neighbours){

        ArrayList<Cell> listOfNeighbours = new ArrayList<>();
        for(int row = 0; row < grid[0].length; row++){
            for(int column = 0; column < grid.length; column++){
                grid[row][column].setOnBound(false);
                listOfNeighbours.clear();
                neighbours.moore(row,column,listOfNeighbours);
                for(int x = 0; x < listOfNeighbours.size(); x++){
                    if(listOfNeighbours.get(x).getColor() != grid[row][column].getColor()){
                        grid[row][column].setOnBound(true);
                        break;
                    }
                }
            }
        }

    }
}
