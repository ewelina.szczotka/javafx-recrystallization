package model;

import controllers.Cell;

import java.util.ArrayList;
import java.util.Random;

public class Neighbours {

    private Random random;
    private int tempRow,tempColumn;
    private int rows, columns, radius;
    private double cellSize;
    private String neighbourhood, bc;
    private Cell[][] grid;


    public Neighbours(Cell[][] grid, String neighbourhood, String bc){
        this.grid = grid;
        this.neighbourhood = neighbourhood;
        this.bc = bc;
        rows = grid[0].length;
        columns = grid.length;
    }

    public Neighbours(Cell[][] grid, String neighbourhood, String bc, double cellSize, int radius){
        this.grid = grid;
        this.neighbourhood = neighbourhood;
        this.bc = bc;
        this.cellSize = cellSize;
        this.radius = radius;
        rows = grid[0].length;
        columns = grid.length;
    }

    public void addNeighbours(ArrayList<Cell> neighbours, int row, int column){

        neighbours.clear();

        if(neighbourhood.equals("von Neumann")) {
            vonNeumann(row,column, neighbours);
        }
        else if(neighbourhood.equals("Moore")){
            moore(row,column, neighbours);
        }
        else if(neighbourhood.equals("heksagonalne prawe")) {
            hexRight(row,column, neighbours);
        }
        else if(neighbourhood.equals("heksagonalne lewe")){
            hexLeft(row,column, neighbours);
        }
        else if(neighbourhood.equals("heksagonalne losowe")) {
            random = new Random();
            int x = random.nextInt(2);
            if(x==0){ hexRight(row,column, neighbours); }
            else{ hexLeft(row,column, neighbours); }
        }
        else if(neighbourhood.equals("pentagonalne losowe")){
            random = new Random();
            int x = random.nextInt(4);
            if(x==0) pentLeft(row,column, neighbours);
            else if(x==1) pentRight(row,column, neighbours);
            else if(x==2) pentDown(row,column, neighbours);
            else pentUp(row,column, neighbours);
        }
        else if(neighbourhood.equals("z promieniem")){
            withRadius(row,column, neighbours);
        }

    }

    private void pentLeft(int row, int column, ArrayList<Cell> neighbours){
        for(int i = -1; i<=1; i++){
            for(int j=-1; j<=0;j++) {
                if(i==0&&j==0){
                    break;
                }
                tempRow = row + i;
                tempColumn = column + j;
                checkBC(row, column, i, j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void pentRight(int row, int column, ArrayList<Cell> neighbours){
        for(int i = -1; i<=1; i++){
            for(int j = 0; j<=1;j++){
                if(i==0&&j==0){
                    break;
                }
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void pentUp(int row, int column, ArrayList<Cell> neighbours){
        for(int i = -1; i<=0; i++){
            for(int j=-1; j<=1;j++) {
                if(i==0&&j==0){
                    break;
                }
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void pentDown(int row, int column, ArrayList<Cell> neighbours){
        for(int i = 0; i<=1; i++){
            for(int j=-1; j<=1;j++) {
                if(i==0&&j==0){
                    break;
                }
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void hexLeft(int row, int column, ArrayList<Cell> neighbours){
        for(int i = -1; i<=1; i++){
            for(int j=-1; j<=1;j++) {
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                if((i!=-1&&j!=1)||(i!=1&&j!=-1)) {
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }
    }

    public void hexRight(int row, int column, ArrayList<Cell> neighbours){

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                if ((i != -1 && j != -1) || (i != 1 && j != 1) ) {
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }
    }

    public void vonNeumann(int row, int column, ArrayList<Cell> neighbours){
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if ((i == 0 && j == -1) || (i == 1 && j == 0) || (i==-1&&j==0)||(i==0&&j==1) ) {
                    tempRow = row+i;
                    tempColumn = column+j;
                    checkBC(row,column,i,j);
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }

    }

    public void moore(int row, int column, ArrayList<Cell> neighbours){

        for(int i = -1; i<=1; i++){
            for(int j=-1; j<=1;j++){
                if(i==0&&j==0){
                    break;
                }
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void withRadius(int row, int column, ArrayList<Cell> neighbours) {

        double width = radius * cellSize;
        double distance;
        int a = radius;
        for (int i = -a; i <= a; i++) {
            for (int j = -a; j <= a; j++) {
                tempRow = row + i;
                tempColumn = column + j;
                checkBC(row, column, i, j);

                double x = grid[row][column].getCenterX() * (cellSize / 2);
                double y = grid[row][column].getCenterY() * (cellSize / 2);
                double x1 = grid[tempRow][tempColumn].getCenterX() * (cellSize / 2);
                double y1 = grid[tempRow][tempColumn].getCenterY() * (cellSize / 2);

                distance = Math.sqrt((Math.pow((x - (x1 + i * cellSize)), 2) + Math.pow((y - (y1 + j * cellSize)), 2)));

                if (distance <= width) {
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }
    }

    public void checkBC(int row, int column, int i, int j){
        if(bc.equals("periodyczne")) {
            if ((row + i) < 0) {
                tempRow = row + i + rows;
            }
            if ((row + i) >= rows) {
                tempRow = row + i - rows;
            }
            if ((column + j) < 0) {
                tempColumn = column + j + columns;
            }
            if ((column + j) >= columns) {
                tempColumn = column + j - columns;
            }
        }
        else if(bc.equals("absorbujące")) {
            if ((row + i) < 0) {
                tempRow = 0;
            }
            if ((row + i) >= rows) {
                tempRow = row;
            }
            if ((column + j) < 0) {
                tempColumn = 0;
            }
            if ((column + j) >= columns) {
                tempColumn = column;
            }
        }

    }

}
